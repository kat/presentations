# Notes from the big brother talk

- ANPR 

- Privacy international
    - brigbrotherincorporated
    - blackberry surveillance ( military has these resources, not expensive )
    - Mobile phone IMEI hoovers ( hoover up all IMEI numbers in a particular area )

- open rights group
    - Communication Capability Project ( was IMP )
    - database state
        - no2id database state
        - what about the id thing that was federated ? mydex ?
    - Companys that want to surveill what we are doing 
        - facebook / google /tesco
        - centralising services
    - behavioural advertising
    - Mentioned tor/ghostery
    

- facebook
    - facebook cookie logs you across websites
    - Are facebook a socialmedia company or advertising company
    - Ditto google
    - supercookie tracks across several sites
        html 5 has persistent storage mechanism that can be used by advertisers.
    - UK government thinks that consent to be tracked can be given before accessing a service.

- Demand for surveillance systems being driven by corporate providers, selling them to states to provide solutions to (non-existent) social problems.

- Phones
    - smartphones are too smart!  If you are really paranoid use an 'operational network' and keep that separate.  You are better to use a really old phone.
    - Android phones - whispersytems, redphone, textphone, k9, apg
    - Truecrypt ??? hidden containers 
 
- Booting pc from USB, windows keeps a log of USB devices that have been plugged in. 

- Germany: there is  a process where you can get police information on you. Political campaign to get information from the police
- Germany: strong privacy movement, that government surveillance technology should be open source.
- UK: not so strong political.  The subject access request is a European law, so we can do it UK
- 
